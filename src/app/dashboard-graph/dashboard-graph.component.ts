import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Link, Node} from '../d3/models';
import {ConnectionService} from '../connection-list/connection.service';

@Component({
  selector: 'app-dashboard-graph',
  templateUrl: './dashboard-graph.component.html',
  styleUrls: ['./dashboard-graph.component.css']
})
export class DashboardGraphComponent implements OnChanges {

  constructor(private connectionService: ConnectionService) {
  }

  @Input() inputNodes;
  @Input() names: boolean;
  @Output()
  changedEvent = new EventEmitter();

  nodes: Node[] = [];
  links: Link[] = [];


  ngOnChanges(changes: SimpleChanges): void {
    this.drawGraph();
  }

  drawGraph() {
    if (this.inputNodes !== undefined && this.names) {
      this.nodes = [];
      this.links = [];
      this.nodes.push(new Node(this.inputNodes._id, this.inputNodes.name, this.inputNodes.nodeType));

      if (this.inputNodes.attended !== undefined) {
        for (let i = 0; i < this.inputNodes.attended.length; i++) {
          this.nodes.push(new Node(this.inputNodes.attended[i]._id, this.inputNodes.attended[i].name, this.inputNodes.attended[i].nodeType));
          this.nodes[0].linkCount++;
          this.nodes[i + 1].linkCount++;
          this.links.push(new Link(this.inputNodes._id, this.inputNodes.attended[i]._id));
        }
      }

      if (this.inputNodes.connected_with !== undefined) {
        for (let i = 0; i < this.inputNodes.connected_with.length; i++) {
          this.nodes.push(new Node(this.inputNodes.connected_with[i]._id, this.inputNodes.connected_with[i].name, this.inputNodes.connected_with[i].nodeType));
          this.nodes[0].linkCount++;
          this.nodes[i + 1].linkCount++;
          this.links.push(new Link(this.inputNodes._id, this.inputNodes.connected_with[i]._id));
        }
      }

      console.log(this.nodes);
      console.log(this.links);
      /*for (let i = 0; i < input.length; i++) {
        if (input[i].connected_with !== undefined) {
          for (let j = 0; j < input[i].connected_with.length; j++) {
            let index = 0;

            const currId = input[i].connected_with[j]._id;
            this.links.push(new Link(input[i]._id, currId));

            index = this.nodes.indexOf(this.nodes.filter(n => n.id === input[i]._id)[0]);
            this.nodes[index].linkCount++;

            index = this.nodes.indexOf(this.nodes.filter(n => n.id === currId)[0]);
            this.nodes[index].linkCount++;
          }
        }
      }*/
    }
  }
}
