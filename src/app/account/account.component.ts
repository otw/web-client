import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthenticationService} from '../authentication/authentication.service';
import {Contact} from '../domain/contact';
import {UpdateContactDto} from '../domain/dto/update-contact-dto';
import {AngularFireStorage} from 'angularfire2/storage';
import {Observable} from 'rxjs';
import {ImageService} from '../image.service';
import {flatMap} from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  contact: UpdateContactDto = new UpdateContactDto('', '', '', '', '', '', '');
  submitted = false;
  downloadURL: Observable<any>;
  private contactsURL = 'contacts/';
  selectedFile = null;
  uploadedUrl: any;

  constructor(private authService: AuthenticationService, private imageService: ImageService) {
  }

  ngOnInit() {
    this.authService.getUser().subscribe(res => {
      this.contact = res.body;
      this.getUserImage();
    });
  }

  getUserImage() {
     this.downloadURL = this.imageService.getContactImage(this.contact, this.contactsURL);
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.contact);

    this.authService.updateUser(this.contact).subscribe(
      res => console.log('updated')
    );

    if (this.selectedFile != null) {
      this.imageService.putContactImage(this.selectedFile, this.contactsURL + this.contact.email);
    }
  }

  onFileSelected(e) {
    this.selectedFile = e.target.files[0];

    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = (_event) => {
      this.uploadedUrl = reader.result;
    };

  }
}
