import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation/navigation.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './authentication/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthGuard} from './authentication/AuthGuard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LandingComponentComponent} from './landing-component/landing-component.component';
import {MaterialModule} from '../../material';

import {AuthInterceptor} from './authentication/authInterceptor';
import {GraphComponent} from './visuals/graph/graph.component';
import {NodeVisualComponent} from './visuals/shared/node/node-visual.component';
import {LinkVisualComponent} from './visuals/shared/link/link-visual.component';
import {ZoomableDirective, DraggableDirective} from './d3/directives';
import {D3Service} from './d3';
import {DashboardGraphComponent} from './dashboard-graph/dashboard-graph.component';

import {DossierComponent} from './dossier/dossier.component';
import {ContactFilterPipe} from './dashboard/contact-filter.pipe';
import {ConnectionListComponent} from './connection-list/connection-list.component';
import {AccountComponent} from './account/account.component';
import {EventListComponent} from './event-list/event-list.component';
import {EventFilterPipe} from './event-list/event-filter.pipe';
import {EventComponent} from './event/event.component';
import {CreateEventDialogComponent} from './create-event-dialog/create-event-dialog.component';

import {AgmCoreModule} from '@agm/core';
import {AngularFireModule} from 'angularfire2';
import {AngularFireStorageModule} from 'angularfire2/storage';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { PotentialListComponent } from './potential-list/potential-list.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    DashboardComponent,
    LandingComponentComponent,
    GraphComponent,
    NodeVisualComponent,
    LinkVisualComponent,
    ZoomableDirective,
    DraggableDirective,
    DashboardGraphComponent,
    LandingComponentComponent,
    DossierComponent,
    ContactFilterPipe,
    ConnectionListComponent,
    AccountComponent,
    ConnectionListComponent,
    EventListComponent,
    EventFilterPipe,
    EventComponent,
    CreateEventDialogComponent,
    PotentialListComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    MaterialModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCk32Y-Cie0kV5UIDxgInlwErFZE2I1BtQ',
      libraries: ['places']
    }),
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyDfVXRsUC73-_MTl7XmPOue8_o5U7YRD24',
      authDomain: 'hermit-1551450555829.firebaseapp.com',
      databaseURL: 'https://hermit-1551450555829.firebaseio.com/',
      projectId: 'hermit-1551450555829',
      storageBucket: 'hermit-1551450555829.appspot.com',
      messagingSenderId: '22038167527'
    }),
    AngularFireStorageModule,
    NgxMaterialTimepickerModule.forRoot()
  ],
  exports: [
    HttpClientModule,
    CommonModule
  ],
  providers: [
    D3Service,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  entryComponents: [CreateEventDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
