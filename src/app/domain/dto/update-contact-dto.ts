export class UpdateContactDto {
  firstName: string;
  lastName: string;
  email: string;
  role: string;
  phoneNumber: string;
  description: string;
  password: string;

  constructor(firstName: string, lastName: string, email: string, role: string, phoneNumber: string, description: string, password: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.role = role;
    this.phoneNumber = phoneNumber;
    this.description = description;
    this.password = password;
  }
}


