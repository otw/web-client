export class ConnectedEvent {
  id: number;
  title: string;
  description: string;
  latitude: number;
  longitude: number;
  radius: number;
  address: string;
  startDate: Date;
  endDate: Date;
  imageUrl: string;

  constructor() {
  }
}

