
export class DossierFile {
  id: number;
  contactId: number;
  time: Date;
  latitude: number;
  longitude: number;
  title: string;
  note: string;


  constructor(id: number, time: Date, latitude: number, longitude: number, title: string, note: string) {
    this.time = time;
    this.latitude = latitude;
    this.longitude = longitude;
    this.title = title;
    this.note = note;
  }
}

