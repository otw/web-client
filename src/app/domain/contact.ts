export class Contact {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  description: string;
  imageUrl: string;


  constructor(id: number, firstName: string, lastName: string, phoneNumber: string, description: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.description = description;
  }
}


