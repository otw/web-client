const CONFIG = {
  N : 100,
  SPECTRUM: [
    'rgb(100,100,100)',
    'rgb(255,111,97)',
    'rgb(77,148,255)'
  ]
};

export default CONFIG;
