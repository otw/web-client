import {Component, OnInit} from '@angular/core';
import {LoginComponent} from '../authentication/login.component';
import {AuthenticationService} from '../authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-landing-component',
  templateUrl: './landing-component.component.html',
  styleUrls: ['./landing-component.component.css']
})
export class LandingComponentComponent implements OnInit {

  constructor(private authService: AuthenticationService,  private router: Router) {
  }

  ngOnInit() {
    console.log(this.authService.isLoggedIn);
    if (this.authService.isLoggedIn) {
      this.router.navigateByUrl('/dashboard');
    }
  }

}
