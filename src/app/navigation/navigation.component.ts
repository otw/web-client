import {AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';
import {Router} from '@angular/router';
import {CreateEventDialogComponent} from '../create-event-dialog/create-event-dialog.component';
import {MatDialog} from '@angular/material';
import {EventService} from '../event/event.service';
import {ConnectedEvent} from '../domain/connected-event';

export interface DialogData {
  id: number;
  title: string;
  address: string;
  startDate: Date;
  endDate: Date;
  latitude: number;
  longitude: number;
  radius: number;
  description: string;
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  @Output() toggleNav = new EventEmitter();
  events: ConnectedEvent[] = [];
  newEvent: ConnectedEvent = new ConnectedEvent();

  constructor(private authenticationService: AuthenticationService, private eventService: EventService, public dialog: MatDialog, private router: Router) {
  }

  toggleNavBar(e) {
    this.toggleNav.emit(e);
  }

  logoutUser() {
    this.authenticationService.logout();
  }

  showDialog() {
    const dialogRef = this.dialog.open(CreateEventDialogComponent, {
      height: 'calc(100% - 40px)',
      width: '650px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.eventService.getEvents().subscribe(
        res => {
          this.events = res.body;
        }
      );
    });
  }
}
