import APP_CONFIG from '../../app.config';

export class Node implements d3.SimulationNodeDatum {
  // Optional - defining optional implementation properties - required for relevant typing assistance
  index?: any;
  name?: any;
  x?: any;
  y?: any;
  vx?: any;
  vy?: any;
  fx?: any;
  fy?: any;

  id: string;
  type: string;
  linkCount: number = 0;


  constructor(id, name, type) {
    this.id = id;
    this.name = name;
    this.type = type;
  }

  get r() {
    return 160;
  }

  get fontSize() {
    return 50 + 'px';
  }

  get color() {
    if (this.type === 'CONTACT') {
      return APP_CONFIG.SPECTRUM[1];
    } else if (this.type === 'EVENT') {
      return APP_CONFIG.SPECTRUM[2];
    } else {
      return APP_CONFIG.SPECTRUM[0];
    }
  }
}
