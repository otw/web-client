import {Injectable} from '@angular/core';
import {AngularFireStorage} from 'angularfire2/storage';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private afStorage: AngularFireStorage) {
  }

  getContactImages(contacts, url) {
    for (const contact of contacts) {
      this.afStorage.ref(url + contact.email).getDownloadURL().subscribe(res => contact.imageUrl = res);
    }
    return contacts;
  }

  getEventImages(events, url) {
    for (const event of events) {
      this.afStorage.ref(url + event.id).getDownloadURL().subscribe(res => event.imageUrl = res);
    }
    return events;
  }

  putContactImage(contact, url) {
    this.afStorage.ref(url).put(contact);
  }

  getContactImage(contact, url): Observable<any> {
    return this.afStorage.ref(url + contact.email).getDownloadURL();
  }

  putEventImage(event, url) {
    this.afStorage.ref(url).put(event.id);
  }
}
