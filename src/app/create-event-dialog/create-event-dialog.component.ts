import {Component, ElementRef, Inject, NgZone, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialogRef} from '@angular/material';
import {DialogData} from '../event/event.component';
import {FormControl} from '@angular/forms';
import {MapsAPILoader} from '@agm/core';
// @ts-ignore
import {} from '@types/googlemaps';
import {ConnectedEvent} from '../domain/connected-event';
import {EventService} from '../event/event.service';
import {AngularFireStorage} from 'angularfire2/storage';
import {ImageService} from '../image.service';

@Component({
  selector: 'app-create-event-dialog',
  templateUrl: './create-event-dialog.component.html',
  styleUrls: ['./create-event-dialog.component.css']
})
export class CreateEventDialogComponent implements OnInit {

  newEvent: ConnectedEvent = new ConnectedEvent();
  zoom = 18;
  locationChosen = false;
  radius = 50;
  eventStartTime;
  eventEndTime;
  isEditing = false;
  selectedFile = null;

  @ViewChild('search')
  public searchElementRef: ElementRef;
  public searchControl: FormControl;
  private eventsURL = '/events';

  constructor(
    public dialogRef: MatDialogRef<CreateEventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private mapsAPILoader: MapsAPILoader,
    private eventService: EventService,
    private imageService: ImageService) {
  }

  ngOnInit() {
    this.initEvent();
    this.searchControl = new FormControl();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: []
      });

      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        this.newEvent.latitude = autocomplete.getPlace().geometry.location.lat();
        this.newEvent.longitude = autocomplete.getPlace().geometry.location.lng();
        this.newEvent.address = autocomplete.getPlace().formatted_address;
        this.locationChosen = true;
        this.zoom = 18;
      });
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChoseLocation(event) {
    this.newEvent.latitude = event.coords.lat;
    this.newEvent.longitude = event.coords.lng;
    this.locationChosen = true;
  }

  changeRadius(e) {
    this.newEvent.radius = e;
  }

  changeCoords(e) {
    this.newEvent.latitude = e.coords.lat;
    this.newEvent.longitude = e.coords.lng;
  }

  registerMapClick(e) {
    this.newEvent.latitude = e.coords.lat;
    this.newEvent.longitude = e.coords.lng;
  }

  createEvent() {
    this.setEventHours();

    if (!this.isEditing) {
      this.eventService.createEvent(this.newEvent).subscribe(
        () => {
          console.log('created');
        }
      );
    } else {
      this.eventService.updateEvent(this.newEvent).subscribe(
        () => {
          console.log('updated');
        }
      );
    }

    this.imageService.putEventImage(this.newEvent, this.eventsURL);

   /* if (this.selectedFile != null) {
      const ref = this.afStorage.ref(this.newEvent.id);
      ref.put(this.selectedFile);
    }*/
  }

  initEvent() {
    if (this.data === null) {
      this.newEvent.latitude = 50.5039;
      this.newEvent.longitude = 4.4699;
      this.newEvent.radius = 50;
      this.newEvent.startDate = new Date();
      this.newEvent.endDate = new Date();
    } else {
      this.isEditing = true;
      this.newEvent.id = this.data.id;
      const startDate = new Date(this.data.startDate);
      const endDate = new Date(this.data.endDate);
      this.newEvent.title = this.data.title;
      this.newEvent.address = this.data.address;
      this.newEvent.latitude = this.data.latitude;
      this.newEvent.longitude = this.data.longitude;
      this.newEvent.radius = this.data.radius;
      this.newEvent.startDate = startDate;
      this.newEvent.endDate = endDate;
      this.newEvent.description = this.data.description;

      this.eventStartTime = startDate.getHours() + ':' + startDate.getMinutes();
      this.eventEndTime = endDate.getHours() + ':' + endDate.getMinutes();
    }
  }

  setEventHours() {
    const startTime = this.eventStartTime.split(':');
    const endTime = this.eventEndTime.split(':');
    this.newEvent.startDate.setHours(startTime[0]);
    this.newEvent.startDate.setMinutes(startTime[1]);

    this.newEvent.endDate.setHours(endTime[0]);
    this.newEvent.endDate.setMinutes(endTime[1]);
  }

  onFileSelected(e) {
    this.selectedFile = e.target.files[0];

    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
  }


}
