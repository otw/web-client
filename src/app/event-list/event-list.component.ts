import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ConnectedEvent} from '../domain/connected-event';
import {EventService} from '../event/event.service';
import {CreateEventDialogComponent} from '../create-event-dialog/create-event-dialog.component';
import {NavigationComponent} from '../navigation/navigation.component';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit, OnChanges {

  @Input() events: ConnectedEvent[];
  @Output() changeDossier = new EventEmitter();
  searchTerm: string;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.events);
  }

  ngOnInit() {

  }

  showDialog() {
  }
}
