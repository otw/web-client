import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'eventFilter'
})
export class EventFilterPipe implements PipeTransform {

  transform(events: any, searchTerm: any): any {
    if (searchTerm === undefined) {
      return events;
    }
    // return updated contacts array
    return events.filter(function (event) {
      return event.title.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

}
