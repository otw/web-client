import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './authentication/login.component';
import {AuthGuard} from './authentication/AuthGuard';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LandingComponentComponent} from './landing-component/landing-component.component';
import {EventComponent} from './event/event.component';
import {AccountComponent} from './account/account.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: LandingComponentComponent},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'events', component: EventComponent},
  {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
  {path: 'register/:id', component: LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
