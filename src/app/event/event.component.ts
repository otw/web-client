import {Component, OnInit} from '@angular/core';
import {ConnectedEvent} from '../domain/connected-event';
import {EventService} from './event.service';
import {MatDialog} from '@angular/material';
import {CreateEventDialogComponent} from '../create-event-dialog/create-event-dialog.component';
import {ImageService} from '../image.service';

export interface DialogData {
  id: number;
  title: string;
  address: string;
  startDate: Date;
  endDate: Date;
  latitude: number;
  longitude: number;
  radius: number;
  description: string;
}

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})

export class EventComponent implements OnInit {
  events: ConnectedEvent[] = [];
  private eventsURL = 'events/';

  constructor(private eventService: EventService, public dialog: MatDialog,
              private imageService: ImageService) {
  }

  ngOnInit() {
    this.eventService.getEvents().subscribe(
      res => {
        this.events = res.body;
        this.events = this.imageService.getEventImages(this.events, this.eventsURL);
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateEventDialogComponent, {
      height: 'calc(100% - 40px)',
      width: '650px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.eventService.getEvents().subscribe(
        res => {
          this.events = res.body;
        }
      );
    });
  }


  openEditDialog(eventData) {
    const dialogRef = this.dialog.open(CreateEventDialogComponent, {
      height: 'calc(100% - 40px)',
      width: '650px',
      data: {id: eventData.id, title: eventData.title, address: eventData.address, latitude: eventData.latitude,
        longitude: eventData.longitude, radius: eventData.radius,
        startDate: eventData.startDate, endDate: eventData.endDate,
        description: eventData.description}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.eventService.getEvents().subscribe(
        res => {
          this.events = res.body;
        }
      );
    });
  }
}

