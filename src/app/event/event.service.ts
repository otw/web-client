import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {ConnectedEvent} from '../domain/connected-event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) {
  }

  getEvents(): Observable<any> {
    return this.http.get(environment.apiUrl + '/event/event/getEvents', {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  createEvent(event: ConnectedEvent): Observable<any> {
    const eventJson = JSON.stringify(event);
    return this.http.post(environment.apiUrl + '/event/event/create', eventJson, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  updateEvent(event: ConnectedEvent): Observable<any> {
    const eventJson = JSON.stringify(event);
    console.log(eventJson);
    return this.http.put(environment.apiUrl + '/event/event/update/' + event.id, eventJson, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }
}
