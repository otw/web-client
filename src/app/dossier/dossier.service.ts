import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {DossierFile} from '../domain/dossierFile';

@Injectable({
  providedIn: 'root'
})
export class DossierService {

  constructor(private http: HttpClient) {
  }

  getDossier(contactId: number): Observable<any> {

    return this.http.get(environment.apiUrl + '/dossier/dossier/getContactDossiers/' + contactId, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  updateDossier(file: DossierFile) {

    const fileJson = JSON.stringify(file);

    return this.http.put(environment.apiUrl + '/dossier/dossier/updateFile', fileJson, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  deleteFile(dossierId: number, fileId: number): Observable<any> {
    return this.http.put(environment.apiUrl + '/dossier/dossier/deleteNoteFromDossierFile/' + 1 + '/' + fileId, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }
}
