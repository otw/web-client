import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Contact} from '../domain/contact';
import {Dossier} from '../domain/dossier';
import {DossierService} from './dossier.service';
import {ImageService} from '../image.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dossier',
  templateUrl: './dossier.component.html',
  styleUrls: ['./dossier.component.css']
})
export class DossierComponent implements OnInit, OnChanges {
  private contactsURL = 'contacts/'
  panelOpenState = false;
  editingPanels: number[] = [];
  downloadURL: Observable<any>;
  @Input()
  contact = new Contact(0, '', '', '', '');
  @Output() close = new EventEmitter();

  initFile: File[];

  @Input()
  dossier = new Dossier();

  constructor(private dossierService: DossierService, private imageService: ImageService) {

  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.contact !== undefined) {
      this.getUserImage();
    }
  }

  getUserImage() {
    this.downloadURL = this.imageService.getContactImage(this.contact, this.contactsURL);
  }

  changeEditMode(fileId: number) {
    const idIndex = this.getIdIndex(fileId);
    if (idIndex > -1) {
      this.deleteItemFromEditingPanels(idIndex);
    } else {
      this.editingPanels.push(fileId);
    }
    console.log(this.editingPanels);
  }

  updateDossier(fileId) {
    let fileToSave;
    for (const dossierFile of this.dossier.file) {
      if (dossierFile.id === fileId) {
        fileToSave = dossierFile;
      }
    }

    const idIndex = this.getIdIndex(fileId);

    if (fileToSave !== undefined) {
      this.dossierService.updateDossier(fileToSave).subscribe(
        () => {
          this.deleteItemFromEditingPanels(idIndex);
        }
      );
    }
  }

  deleteFile(fileId) {
    this.dossierService.deleteFile(1, fileId).subscribe(
      () => {
        this.deleteNoteFromDossierFile(fileId);
        this.deleteItemFromEditingPanels(this.getIdIndex(fileId));
      }
    );
  }

  isEditing(fileId: number): boolean {
    return this.editingPanels.indexOf(fileId) > -1;
  }

  deleteItemFromEditingPanels(index: number) {
    this.editingPanels.splice(index, 1);
  }

  deleteNoteFromDossierFile(fileId: number) {
    const fileIndex = this.dossier.file.findIndex(f => f.id === fileId);
    if (fileIndex > -1) {
      this.dossier.file[fileIndex].note = '';
    }
  }

  getIdIndex(fileId: number): number {
    return this.editingPanels.indexOf(fileId);
  }

  closeDossier() {
    this.close.emit();
  }
}
