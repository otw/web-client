import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConnectedEvent} from '../domain/connected-event';

@Component({
  selector: 'app-potential-list',
  templateUrl: './potential-list.component.html',
  styleUrls: ['./potential-list.component.css']
})
export class PotentialListComponent implements OnInit {
  @Input() events: ConnectedEvent[];
  @Output() changeDossier = new EventEmitter();
  searchTerm: string;

  constructor() { }

  ngOnInit() {
  }

}
