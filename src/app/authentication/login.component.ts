import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  firstName = '';
  lastName = '';
  email = '';
  password = '';
  isLogin = true;

  constructor(private authService: AuthenticationService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn) {
      this.router.navigateByUrl('/dashboard');
    }
    if (this.activatedRoute.snapshot.params['id'] !== undefined) {
      this.isLogin = false;
    }
  }

  loginUser() {

    this.isLogin ?
      this.authService.login(this.email, this.password).subscribe(
        () => {
          this.router.navigateByUrl('/dashboard');
        }
      )
      :
      this.authService.register(this.firstName, this.lastName, this.email, this.password).subscribe(
        () => {
          this.changeAction();
        }
      );

  }

  changeAction() {
    this.isLogin = !this.isLogin;
  }
}
