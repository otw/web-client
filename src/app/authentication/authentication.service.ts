import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Contact} from '../domain/contact';
import {UpdateContactDto} from '../domain/dto/update-contact-dto';


export const TOKEN_NAME = 'jwt_token';
const httpOptions = {
  headers: new HttpHeaders({'Accept': 'application.json', 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  // private loggedIn = new BehaviorSubject<boolean>(false);
  loggedIn: boolean;

  constructor(private http: HttpClient) {
  }

  isTokenExpired() {
    // TODO expiry date
    if (this.getToken() !== null) {
      this.loggedIn = true;
      return false;
    }

    this.loggedIn = false;
    return true;
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token.split('Bearer')[1]);
  }

  getUser(): Observable<any> {
    return this.http.get(environment.apiUrl + '/contact/contact/user', {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  updateUser(contact: UpdateContactDto) {
    const contactJSON = JSON.stringify(contact);
    console.log(contactJSON);
    return this.http.put(environment.apiUrl + '/contact/contact/update', contactJSON, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  get isLoggedIn() {
    this.checkIfLoggedIn();
    return this.loggedIn;
  }

  checkIfLoggedIn() {
    if (this.isTokenExpired()) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
  }

  logout() {
    this.loggedIn = false;
    localStorage.removeItem(TOKEN_NAME);
  }

  login(userMail, userPass): Observable<any> {
    const loginUser = {
      email: userMail,
      password: userPass
    };

    return this.http.post(environment.apiUrl + '/contact/auth', loginUser, {
      observe: 'response',
      headers: new HttpHeaders({
        'Accept': 'application.json',
        'Content-Type': 'application/json',
      })
    }).pipe(
      tap(res => this.setToken(res.headers.get('Authorization')))
    ).pipe(
      tap(() => this.loggedIn = true)
    );
  }

  register(firstName: string, lastName: string, email: string, password: string) {
    const registerUser = {
      email: email,
      password: password,
      firstName: firstName,
      lastName: lastName
    };
    return this.http.post(environment.apiUrl + '/contact/contact/registration', registerUser, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }
}
