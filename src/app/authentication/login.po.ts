import {browser, by, element} from 'protractor';

export class LoginPo {
  navigateTo() {
    return browser.get('/login');
  }

  getTitleText() {
    return element(by.css('mat-card-title')).getText();
  }

  getFormElementByName(name) {
    return element(by.name(name));
  }

  getFormTitle() {
    return element(by.css('mat-card-title'));
  }

  getForm() {
    return element(by.css('form'));
  }

  getElementById(id) {
    return element(by.css(id));
  }
}
