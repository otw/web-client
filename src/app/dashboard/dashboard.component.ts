import {Component, OnInit} from '@angular/core';
import {Node, Link} from '../d3/models';
import APP_CONFIG from '../app.config';
import {ConnectionService} from '../connection-list/connection.service';
import {AuthenticationService} from '../authentication/authentication.service';
import {Contact} from '../domain/contact';
import {DossierService} from '../dossier/dossier.service';
import {Dossier} from '../domain/dossier';
import {EventService} from '../event/event.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  nodes: Node[];
  contacts: Contact[] = [];
  eventsWithInfo: Event[] = [];
  selectedContact: Contact;
  events: Event[] = [];
  dossier: Dossier;
  changed = false;
  inputNodes = null;

  constructor(private connectionService: ConnectionService, private dossierService: DossierService, private eventService: EventService) {
  }

  ngOnInit() {
    this.getNodes();
    this.getEventsWithInfo();
    this.getEvents();
    this.connectionService.getNodes().subscribe(res => {
        this.nodes = res.body;
        this.assignNames();
      },
      err => console.log('No connections')
    );
  }

  assignNames() {
    let contactNames = false;
    let eventNames = false;
    const eventids = [];
    const contactids = [];
    this.inputNodes = this.nodes;
    contactids.push(this.inputNodes.contactId);

    if (this.inputNodes.connected_with !== undefined) {
      for (const contactNode of this.inputNodes.connected_with) {
        contactids.push(contactNode.contactId);
      }
    }

    if (this.inputNodes.attended !== undefined) {
      for (const eventNode of this.inputNodes.attended) {
        eventids.push(eventNode.eventId);
      }
    }


    if (this.inputNodes !== undefined) {
      this.connectionService.getContactNames(contactids.toString()).subscribe(res => {
          if (res.body.length > 0) {
            this.inputNodes.name = res.body[0].firstName;
            const nodes = this.inputNodes.connected_with;
            for (let i = 0; i < this.inputNodes.connected_with.length; i++) {
              const result = res.body.filter(function (el) {
                return el.nodeId === nodes[i].contactId && nodes[i].nodeType === 'CONTACT';
              });
              if (result !== undefined && result.length > 0) {
                this.inputNodes.connected_with[i].name = result[0].firstName;
              }
            }
            this.nodes = this.inputNodes;
            contactNames = true;
            this.checkNames(contactNames, eventNames);
          }
        },
        err => console.log('No connections'));

      this.connectionService.getEventNames(eventids.toString()).subscribe(response => {
          if (response.body.length > 0) {
            const nodes = this.inputNodes.attended;
            for (let i = 0; i < this.inputNodes.attended.length; i++) {
              const result = response.body.filter(function (el) {
                return el.nodeId === nodes[i].eventId && nodes[i].nodeType === 'EVENT';
              });
              if (result !== undefined && result.length > 0) {
                this.inputNodes.attended[i].name = result[0].title;
              }
            }
            console.log(this.inputNodes);
            this.nodes = this.inputNodes;
            eventNames = true;
            this.checkNames(contactNames, eventNames);
          } else {
            eventNames = true;
          }
        },
        err => console.log('No connections')
      );
    }
  }

  getNodes() {
    this.connectionService.getNodesWithInfo().subscribe(res => {
        this.contacts = res.body;
      },
      err => console.log('No contacts'));
  }

  getEvents() {
    this.eventService.getEvents().subscribe(res => {
        this.events = res.body;
      },
      err => console.log('No events'));
  }

  getEventsWithInfo() {
    this.connectionService.getEvents().subscribe(
      res => {
        this.eventsWithInfo = res.body;
      },
      err => console.log('No events')
    );
  }

  changeSelected($event) {
    // @ts-ignore
    const contactId = $event.contactId;
    this.selectedContact = this.contacts.find(c => c.id === +contactId);

    this.dossierService.getDossier(+contactId).subscribe(
      res => {
        this.dossier = res.body;

      });
  }

  closeDossier() {
    this.selectedContact = null;
  }

  checkNames(contact: boolean, events: boolean) {
    this.changed = contact && events;
  }

}
