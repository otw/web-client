import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'contactFilter'
})
export class ContactFilterPipe implements PipeTransform {

  transform(contacts: any, searchTerm: any): any {
    // check if searchterm is undefined
    if (searchTerm === undefined) {
      return contacts;
    }
    // return updated contacts array
    return contacts.filter(function (contact) {
      return contact.firstName.toLowerCase().includes(searchTerm.toLowerCase())
        || contact.lastName.toLowerCase().includes(searchTerm.toLowerCase())
        || contact.phoneNumber.toLowerCase().includes(searchTerm.toLowerCase());
    });

  }

}
