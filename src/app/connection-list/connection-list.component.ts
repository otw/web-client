import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ConnectionService} from './connection.service';
import {Contact} from '../domain/contact';
import {ImageService} from '../image.service';

@Component({
  selector: 'app-connection-list',
  templateUrl: './connection-list.component.html',
  styleUrls: ['./connection-list.component.css']
})
export class ConnectionListComponent implements OnInit, OnChanges {

  @Input() contacts: Contact[];
  @Output() changeDossier = new EventEmitter();
  searchTerm: string;
  private contactURL = 'contacts/';

  constructor(private connectionService: ConnectionService, private imageService: ImageService) {
  }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.contacts != null) {
      this.contacts = this.imageService.getContactImages(this.contacts, this.contactURL);
    }
  }

  deleteConnection(contactId: number) {
    this.connectionService.deleteConnection(contactId).subscribe(
      () => {
        this.deleteContactLocal(contactId);
      }
    );
  }

  emitChangeDossier(event, contactId) {
    this.changeDossier.emit({event: event, contactId: contactId});
  }

  deleteContactLocal(contactId: number) {
    const contactIndex = this.contacts.findIndex(c => c.id === contactId);
    if (contactIndex > -1) {
      this.contacts.splice(contactIndex, 1);
    }
  }
}
