import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor(private http: HttpClient) {
  }

  getNodesWithInfo(): Observable<any> {

    return this.http.get(environment.apiUrl + '/connection/connection/userConnectionsWithInfo', {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  getNodes(): Observable<any> {
    return this.http.get(environment.apiUrl + '/connection/connection/userConnections', {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  getContactNames(contactids): Observable<any> {
    return this.http.get(environment.apiUrl + '/contact/contact/connectedNames?userids=' + contactids, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  getEventNames(eventids): Observable<any> {
    return this.http.get(environment.apiUrl + '/event/event/connectedEventNames?eventids=' + eventids, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  getEvents(): Observable<any> {
    return this.http.get(environment.apiUrl + '/connection/connection/userEventsWithInfo', {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    });
  }

  deleteConnection(connectionId: number) {
    return this.http.delete(environment.apiUrl + '/connection/connection/deleteConnection/' + connectionId, {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      tap(res => console.log(res))
    );
  }
}
