import { AppPage } from './app.po';
import {LoginPo} from '../../src/app/authentication/login.po';
import {Log} from '@angular/core/testing/src/logger';
import {TOKEN_NAME} from '../../src/app/authentication/authentication.service';
import {browser} from 'protractor';

describe('Authentication tests', () => {
  let page: LoginPo;

  beforeEach(() => {
    page = new LoginPo();
  });

  it('Form should have login title', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Login');
  });

  it('Login form should be invalid', () => {
    page.getFormElementByName('email').sendKeys('');
    page.getFormElementByName('password').sendKeys('');

    const form = page.getForm().getAttribute('class');

    expect(form).toContain('ng-invalid');

  });

  it('Login form should be valid', () => {
    page.getFormElementByName('email').sendKeys('testing@gmail.com');
    page.getFormElementByName('password').sendKeys('12345');

    const form = page.getForm().getAttribute('class');

    expect(form).toContain('ng-valid');

  });

  it('User should be logged in', () => {
    page.getFormElementByName('email').clear();
    page.getFormElementByName('password').clear();
    page.getFormElementByName('email').sendKeys('testing@gmail.com');
    page.getFormElementByName('password').sendKeys('12345');

    const form = page.getForm().getAttribute('class');
    page.getElementById('#btnLogin').click();
    const valLocalStorage = browser.executeScript('return window.localStorage.getItem(\'jwt_token\');');

    expect(valLocalStorage).toBeDefined();
  });


});
